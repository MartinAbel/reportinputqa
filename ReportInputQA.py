# import necessary libraries
from __future__ import division
import os
from glob import glob
from math import log10, floor
import pandas as pd


class GetStatsOnInputData():
    """
    This class contains useful functions to compute stats about input files
    """

    @staticmethod
    def error_files(path):
        """
        Get count of how many error files there were within the given input path
        """
        # initialize variables
        num_error_files = 0
        len_error_file_list = 0
        try:
            # get all the files in the given directory who contain err in their file name
            error_file_list = glob(path + '*.err')
            # now check if any are non-empty
            len_error_file_list = len(error_file_list)
            if len_error_file_list != 0:
                for file_p in error_file_list:
                    if os.path.getsize(file_p) > 0:
                        num_error_files += 1
        except Exception, e:
            print 'Exception occurred while calling function count_error_files'
            print 'Details on exception: ' + str(e)
        return {'num_error_files': num_error_files, 'total_num_files': len_error_file_list}

    @staticmethod
    def count_missing_values_per_column(input_df):
        """
        This function counts how many missing values there are per column in the given input Pandas dataframe
        """
        return len(input_df.index) - input_df.count()

    @staticmethod
    def get_stats(input_df):
        return input_df.describe()

    def __init__(self):
        # call function error files
        self.error_files = self.error_files('../moment6/outdir/')
        self.num_error_files = self.error_files['num_error_files']
        self.percentage_error_files = self.error_files['num_error_files'] / self.error_files['total_num_files']
        # read concatenated csv file
        concat_df = pd.read_csv('../moment6/outdir/output.tsv')
        self.missing_values_per_column = self.count_missing_values_per_column(concat_df)
        self.stats = self.get_stats(concat_df)


def round_to_sig(x, sig=2):
    """
    Function to round a float x to a given number of significant digits sig
    """
    return round(x, sig - int(floor(log10(x))) - 1)


X = GetStatsOnInputData()
print 'Number of error files found: {0}, corresponding to {1}%'.format(str(X.num_error_files),
                                                                       str(round_to_sig(X.percentage_error_files, 2)))
print 'Missing values per column: '+str(self.missing_values_per_column)
print 'Column statistics: '+str(self.stats)
